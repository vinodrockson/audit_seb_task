var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/', {
	      controller: 'FindController',
	      templateUrl: 'views/audit/list.html'
	    })
	    .when('/finrep', {
	      controller: 'ReportController',
	      templateUrl: 'views/audit/finrep.html'
	    })
	    .when('/contact', {
	      templateUrl: 'views/audit/contact.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});