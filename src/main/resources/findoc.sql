insert into customer (id, name, address) values (101, 'Vinod', 'Raatuse-22,Tartu');
insert into customer (id, name, address) values (145, 'Margus', 'Akademia34,Tallinn');
insert into customer (id, name, address) values (278, 'Kristi', 'Narva Mnt,Tartu');
insert into customer (id, name, address) values (457, 'Helen', 'Raatuse22,Tartu');
insert into customer (id, name, address) values (260, 'Prashanth', 'Parnu mnt,Tallinn');
insert into customer (id, name, address) values (845, 'Marie', 'Ulemiste 23,Tallinn');
insert into customer (id, name, address) values (620, 'Jillu', 'Loost 23, Tallinn');
insert into customer (id, name, address) values (904, 'Kaarel', 'Viljandi');
insert into customer (id, name, address) values (673, 'Darlu', 'Ulemiste 45,Tallinn');
insert into document (id, description) values (12435, 'This is a document 12435');
insert into document (id, description) values (78237, 'This is a document 78237');
insert into document (id, description) values (23493, 'This is a document 23493');
insert into document (id, description) values (33873, 'This is a document 33873');
insert into document (id, description) values (23947, 'This is a document 23947');
insert into document (id, description) values (36748, 'This is a document 36748');
insert into document (id, description) values (27834, 'This is a document 27834');
insert into document (id, description) values (34342, 'This is a document 34342');
insert into document (id, description) values (93746, 'This is a document 93746');
insert into document (id, description) values (22328, 'This is a document 22328');
insert into document (id, description) values (39734, 'This is a document 39234');
insert into document (id, description) values (13424, 'This is a document 13424');
insert into document (id, description) values (32424, 'This is a document 32424');
insert into document (id, description) values (23442, 'This is a document 23442');
insert into document (id, description) values (23424, 'This is a document 23424');
insert into document (id, description) values (56455, 'This is a document 56455');
insert into document (id, description) values (56456, 'This is a document 56456');
insert into document (id, description) values (29837, 'This is a document 29837');
insert into document (id, description) values (23444, 'This is a document 23444');
insert into document (id, description) values (33244, 'This is a document 33244');
insert into document (id, description) values (87577, 'This is a document 87577');
insert into document (id, description) values (67665, 'This is a document 67665');
insert into document (id, description) values (54545, 'This is a document 54545');
insert into document (id, description) values (56474, 'This is a document 56474');
insert into document (id, description) values (82346, 'This is a document 82346');
insert into document (id, description) values (19823, 'This is a document 19823');
insert into document (id, description) values (39234, 'This is a document 39234');
insert into document (id, description) values (32734, 'This is a document 32734');
insert into document (id, description) values (24364, 'This is a document 24364');
insert into document (id, description) values (53475, 'This is a document 53475');
insert into document (id, description) values (34593, 'This is a document 34593');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (1, (select id from document where description='This is a document 33873'), (select id from customer where name='Vinod'), '23-02-2015', 'SEB', 1, 6, '23-02-2015','22-08-2015'); 
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (2, (select id from document where description='This is a document 33244'), (select id from customer where name='Margus'), '04-12-2014', 'SEB', 1, 12, '04-12-2014','04-12-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (3, (select id from document where description='This is a document 82346'), (select id from customer where name='Prashanth'), '15-01-2014', 'SEB', 2, 9, '15-01-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (4, (select id from document where description='This is a document 87577'), (select id from customer where name='Helen'), '29-08-2013', 'SEB', 1, 13, '29-08-2013','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (5, (select id from document where description='This is a document 34593'), (select id from customer where name='Marie'), '11-11-2014', 'SEB', 3, 14, '11-11-2014','08-02-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (6, (select id from document where description='This is a document 78237'), (select id from customer where name='Kaarel'), '19-05-2014', 'SEB', 2, 8, '19-05-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (7, (select id from document where description='This is a document 56456'), (select id from customer where name='Jillu'), '23-12-2012', 'SEB', 1, 17, '23-12-2012','20-08-2014');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (8, (select id from document where description='This is a document 27834'), (select id from customer where name='Vinod'), '02-04-2014', 'SEB', 1, 19, '02-04-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (9, (select id from document where description='This is a document 19823'), (select id from customer where name='Darlu'), '13-03-2015', 'SEB', 3, 24, '13-03-2015','12-03-2017');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (10, (select id from document where description='This is a document 13424'), (select id from customer where name='Kristi'), '07-10-2014', 'SEB', 3, 13, '07-10-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (11, (select id from document where description='This is a document 78237'), (select id from customer where name='Kristi'), '18-01-2015', 'Microsoft', 1, 12, '18-01-2015','17-01-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (12, (select id from document where description='This is a document 34342'), (select id from customer where name='Prashanth'), '13-10-2013', 'Microsoft', 2, 9, '13-10-2013','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (13, (select id from document where description='This is a document 32424'), (select id from customer where name='Vinod'), '17-11-2014', 'Microsoft', 3, 17, '17-11-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (14, (select id from document where description='This is a document 19823'), (select id from customer where name='Jillu'), '19-09-2014', 'Microsoft', 1, 18, '19-09-2014','16-04-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (15, (select id from document where description='This is a document 33873'), (select id from customer where name='Darlu'), '03-10-2014', 'Microsoft', 1, 19, '03-10-2014','02-04-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (16, (select id from document where description='This is a document 19823'), (select id from customer where name='Kaarel'), '21-08-2014', 'Microsoft', 2, 13, '21-08-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (17, (select id from document where description='This is a document 13424'), (select id from customer where name='Margus'), '21-08-2014', 'Microsoft', 2, 15, '21-08-2014','21-12-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (18, (select id from document where description='This is a document 67665'), (select id from customer where name='Jillu'), '13-04-2015', 'Nortal', 1, 12, '13-04-2015','13-04-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (19, (select id from document where description='This is a document 12435'), (select id from customer where name='Prashanth'), '28-09-2014', 'Nortal', 3, 18, '28-09-2014','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (20, (select id from document where description='This is a document 22328'), (select id from customer where name='Kaarel'), '11-01-2015', 'Nortal', 2, 7, '11-01-2015','10-08-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (21, (select id from document where description='This is a document 87577'), (select id from customer where name='Vinod'), '01-11-2014', 'Stockmann', 2, 10, '01-11-2014','01-09-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (22, (select id from document where description='This is a document 34593'), (select id from customer where name='Margus'), '11-04-2013', 'Stockmann', 1, 19, '11-04-2013','10-01-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (23, (select id from document where description='This is a document 67665'), (select id from customer where name='Darlu'), '19-12-2012', 'Stockmann', 1, 8, '19-12-2012','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (24, (select id from document where description='This is a document 32734'), (select id from customer where name='Marie'), '20-10-2014', 'Stockmann', 3, 16, '20-10-2014','19-12-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (25, (select id from document where description='This is a document 93746'), (select id from customer where name='Helen'), '14-02-2015', 'Stockmann', 1, 12, '14-02-2015','14-02-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (26, (select id from document where description='This is a document 23442'), (select id from customer where name='Helen'), '23-08-2014', 'Transferwise', 1, 16, '23-08-2014','23-03-2015');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (27, (select id from document where description='This is a document 53475'), (select id from customer where name='Kaarel'), '01-02-2015', 'Transferwise', 3, 11, '01-02-2015','01-01-2016');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (28, (select id from document where description='This is a document 39234'), (select id from customer where name='Darlu'), '28-09-2012', 'Transferwise', 2, 18, '28-09-2012','31-12-9999');
insert into findoc (id, doc_id, customer_id, financial_date, company_name, flg_audit, no_months, valid_from, valid_to) values (29, (select id from document where description='This is a document 54545'), (select id from customer where name='Kristi'), '09-03-2015', 'Transferwise', 1, 15, '09-03-2015','08-11-2016');