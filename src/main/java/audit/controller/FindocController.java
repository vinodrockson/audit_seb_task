package audit.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import audit.assembler.FindocAssembler;
import audit.model.Findoc;
import audit.repositories.FindocRepository;
import audit.resources.FindocResource;

@RestController
@RequestMapping(value = "/rest/doc")
public class FindocController {

	@Autowired
	FindocRepository frepo;
	
	//Gets the list of all hotels from the database
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ResponseStatus(HttpStatus.OK)
	public List<FindocResource> getAll() {
		List<Findoc> findoc = frepo.findAll();
		FindocAssembler assembler = new FindocAssembler();
		List<FindocResource> res = assembler.toResource(findoc);
		return res;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/query")
	@ResponseStatus(HttpStatus.OK)
	public List<FindocResource> listReports(@RequestParam("companyName") String name) throws Exception{
			
			if(name.startsWith("All")){
				List<Findoc> findoc = frepo.findAllMod();
				FindocAssembler assembler = new FindocAssembler();
				List<FindocResource> res = assembler.toResource(findoc);
				return res;
				
			}
			else {
				List<Findoc> findoc = frepo.findByname(name);
				FindocAssembler assembler = new FindocAssembler();
				List<FindocResource> res = assembler.toResource(findoc);
				return res;				
			}
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/find")
	@ResponseStatus(HttpStatus.OK)
	public List<FindocResource> findDoc(@RequestParam("companyName") String name) throws Exception{
			
			if(name.startsWith("All")){
				Date date = new Date();
				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				String curdate= dmyFormat.format(date);
				List<Findoc> findoc = frepo.findAllDoc(curdate);
				List<Findoc> findoc2 = new ArrayList<>();
				if(findoc.size()>1){					
					for (Findoc doc : findoc) {
						if(doc.getFlgAudit()==1){
							findoc2.add(doc);}
					}					
				}
				FindocAssembler assembler = new FindocAssembler();
				List<FindocResource> res = assembler.toResource(findoc2);
				return res;
				
			}
			else {
				Date date = new Date();
				SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
				String curdate= dmyFormat.format(date);
				List<Findoc> findoc = frepo.findDoc(name, curdate);
				List<Findoc> findoc2 = new ArrayList<>();
				if(findoc.size()>1){					
					for (Findoc doc : findoc) {
						if(doc.getFlgAudit()==1){
							findoc2.add(doc);}
					}					
				}
				FindocAssembler assembler = new FindocAssembler();
				List<FindocResource> res = assembler.toResource(findoc2);
				return res;				
			}
	}
}
