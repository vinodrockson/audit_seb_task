package audit.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import audit.model.Findoc;

@Repository
public interface FindocRepository extends JpaRepository<Findoc, Long> {
	
	@Query("select d from Findoc d ORDER BY flgAudit ASC, financialDate DESC")
	List<Findoc> findAllMod();
	
	@Query("select d from Findoc d where d.companyName = :name ORDER BY flgAudit ASC, financialDate DESC")
	List<Findoc> findByname(@Param("name") String name);
	
	@Query("select d from Findoc d where d.companyName = :name and noMonths BETWEEN 12 AND 18 and ((DATE_PART('year', date(:date)) - DATE_PART('year', financialDate)) * 12 + (DATE_PART('month', date(:date)) - DATE_PART('month', financialDate))) <= 18")
	List<Findoc> findDoc(@Param("name") String name, @Param("date") String date);
	
	@Query("select d from Findoc d where noMonths BETWEEN 12 AND 18 and ((DATE_PART('year', date(:date)) - DATE_PART('year', financialDate)) * 12 + (DATE_PART('month', date(:date)) - DATE_PART('month', financialDate))) <= 18")
	List<Findoc> findAllDoc(@Param("date") String date);
}
