package audit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import audit.model.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

}
