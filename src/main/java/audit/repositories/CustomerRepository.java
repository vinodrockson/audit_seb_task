package audit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import audit.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
