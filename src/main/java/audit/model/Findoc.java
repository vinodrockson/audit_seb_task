package audit.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class Findoc {
	
	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	Document doc;
	
	@OneToOne
	Customer customer;
	
	@Temporal(TemporalType.DATE)
	Date financialDate;
	
	String companyName;
	
	int flgAudit;
	int noMonths;
	
	@Temporal(TemporalType.DATE)
	Date validFrom;
	
	@Temporal(TemporalType.DATE)
	Date validTo;
	
}
