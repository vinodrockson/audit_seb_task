package audit.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import audit.controller.FindocController;
import audit.model.Findoc;
import audit.resources.FindocResource;

public class FindocAssembler extends ResourceAssemblerSupport<Findoc, FindocResource> {
	
	public FindocAssembler() {
		super(FindocController.class, FindocResource.class);
		}
	
	//converts to resource
	@Override
	public FindocResource toResource(Findoc doc) {
		FindocResource docResource = createResourceWithId(
			doc.getId(), doc);

		docResource.setIdres(doc.getId());
		docResource.setDoc(doc.getDoc());
		docResource.setCustomer(doc.getCustomer());
		docResource.setCompanyName(doc.getCompanyName());
		docResource.setFinancialDate(doc.getFinancialDate());
		docResource.setFlgAudit(doc.getFlgAudit());
		docResource.setNoMonths(doc.getNoMonths());
		docResource.setValidFrom(doc.getValidFrom());
		docResource.setValidTo(doc.getValidTo());
		return docResource;
		
	}

	//converts list to resource
	public List<FindocResource> toResource(List<Findoc> docList) {
	List<FindocResource> docRes = new ArrayList<>();
	
	for (Findoc doc : docList) {
		docRes.add(toResource(doc));
	}
	
	return docRes;
	}
}