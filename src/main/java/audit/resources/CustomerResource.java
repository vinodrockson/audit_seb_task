package audit.resources;

import lombok.Getter;
import lombok.Setter;
import audit.support.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResource extends ResourceSupport  {

	Long idres;	
	String name;
	String address;
}
