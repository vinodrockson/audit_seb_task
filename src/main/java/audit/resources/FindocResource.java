package audit.resources;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import audit.model.Customer;
import audit.model.Document;
import audit.support.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FindocResource extends ResourceSupport {

	Long idres;
	Document doc;
	Customer customer;
	
	@Temporal(TemporalType.DATE)
	Date financialDate;
	
	String companyName;
	
	int flgAudit;
	int noMonths;
	
	@Temporal(TemporalType.DATE)
	Date validFrom;
	
	@Temporal(TemporalType.DATE)
	Date validTo;	

}
