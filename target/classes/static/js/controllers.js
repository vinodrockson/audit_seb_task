var app = angular.module('project');

app.controller('FindController', function($scope, $http, $route, $location) {
	$scope.hotels = [];
	$http.get('/rest/doc').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.docs = data;
	});
	
});

app.controller('ReportController', function($scope, $http, $location) {
	$scope.docs = [];
	$scope.companyName = '';
	
	console.log($location.path() == "/");
	$scope.catalogShown = false;
	
	$scope.execQuery = function () {
	        $http.get('/rest/doc/query', {params: {companyName: $scope.companyName}}).success(function(data, status, headers, config) {
	        		if ((data || []).length === 0) {
	        		    alert('No Data Available. Try with another selection');
	        		}
	        		else{
			            $scope.docs = data;
			            console.log(JSON.stringify(data));
			            $scope.catalogShown = true;
	        		}
	        });
    };
	
 
    $scope.showDoc = function (company) {
        $http.get('/rest/doc/find', {params: {companyName: company}}).success(function(data, status, headers, config) {
    		if ((data || []).length === 0) {
    		    alert('No Documents to show');
    		}
    		else{
	            $scope.documents = data;
	            console.log(JSON.stringify(data));
	            $scope.catalogShown = true;
    		}
    });

	};
});

